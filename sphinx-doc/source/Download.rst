Download
========

.. graphviz:: dot/Download.dot

Current Version FlowchartWiki 1.2.5
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(If you are running an older version, please see the upgrade info in the
release-notes below.)

There are two versions of the files available:

-  a .zip file for Windows installations
-  a .tar.gz file for Linux/Unix installations

The contents of both files are the same, just the packaging is
different.

-  FlowchartWiki

   -  Download from BitBucket Releases as .zip or .gz:
      `BitBucket <https://bitbucket.org/tkock/flowchartwiki/downloads/?tab=tags>`__

-  PDFbook (our version of the extension is currently broken.)
-  Git Repository

::

     git clone https://bitbucket.org/tkock/flowchartwiki.git
     git clone https://bitbucket.org/tkock/pdfbook.git

Current versions are tagged, you can do
``git checkout flowchartwiki-1.2.5``


Installation prerequisites
~~~~~~~~~~~~~~~~~~~~~~~~~~

-  FlowchartWiki and MediaWiki are based on the "LAMP" Stack:
   Linux/Unix/Windows, Apache, mySQL/Postgres, PHP. This plattform
   should be available on your system.
-  FlowchartWiki is an Extension for MediaWiki. Please install MediaWiki
   first, then add the FlowchartWiki Extension. (See also :doc:`Installation`)
-  The standard installation of FlowchartWiki will NOT work together
   with Semantic MediaWiki (SMW) installed in the same wiki. (See
   :ref:`Installation:Parallel usage with Semantic MediaWiki`
   for a workaround.)

Unpacking and installing
------------------------

Please see :doc:`Installation`

Release and upgrade Notes - Current Release FlowchartWiki
---------------------------------------------------------

FlowchartWiki 1.2.5
~~~~~~~~~~~~~~~~~~~

-  Fixes for deprecations in MW 1.27 and 1.29.1.

FlowchartWiki 1.2.4
~~~~~~~~~~~~~~~~~~~

-  fix sloppy coding that PHP 7.x rejects or complains about now (Thanks
   to Adam C. from the US.)

FlowchartWiki 1.2.3
~~~~~~~~~~~~~~~~~~~

-  fixed MediaWiki 1.27 LTS compatibility issues: 'wfMsg(...)' to
   'wfMessage(...)->text()'

FlowchartWiki 1.2.2
~~~~~~~~~~~~~~~~~~~

-  fixed MediaWiki 1.24 compatibility issues.

Upgrade from 1.2.1:

-  Replace the extension directory with the new version and (optionally)
   delete the images in ``./images/flowchartwiki``

FlowchartWiki 1.2.1
~~~~~~~~~~~~~~~~~~~

-  Changes for deprecated functions in MediaWiki 1.21.x

   -  ExtensionMessages was removed.
   -  counter.php was removed -> replaced by direct copy of old
      function.

-  dot not creating image files with empty label=\ *, needs to be
   label=' ' instead.*
-  added call to PHP clearstatcache()

Upgrade from 1.2.0:

-  Replace the extension directory with the new version and (optionally)
   delete the images in ``./images/flowchartwiki``

FlowchartWiki 1.2.0
~~~~~~~~~~~~~~~~~~~

-  Bugfix by Hiroyuki S.: long pagenames in fchw\_LoadPages() in lib.php
-  catching some php-Notice "undefined index" errors in lib.php with
   isset()

Upgrade from 1.1.x or 1.2.0-beta-x:

-  Replace the extension directory with the new version and delete the
   images in ./images/flowchartwiki
-  optional: Use the new "Category" as Graph Header feature (see :doc:`Customizing`).

FlowchartWiki 1.2.0-beta2
~~~~~~~~~~~~~~~~~~~~~~~~~

-  Bugfix by Gerrit I. - double entries in Database when tags are twice
   in a wikipage
-  PHP 5.3 compatibility - tested together with Peter v.L.
-  Changed field length in schema\_mysql.sql for from\_title and
   relation from 255 to 120 due to problems with UTF8 databases and
   indices being longer than 1024.
-  added $wgExtensionCredits

FlowchartWiki 1.2.0-beta1
~~~~~~~~~~~~~~~~~~~~~~~~~

-  major rebuild of .dot file creation, reduced Database-accesses and
   moved data-structure into internal object hierarchy (see
   fchwobjects.php for the objects)
-  This was a prerequsite for adding other features to the graph:
-  Include a Link to the Category-Page on top of the graph, if
   Customizing has a Page-Type "Category".
-  Position all Pages that have no Level::xxxx assigned at the bottom of
   the graph, sorted in alphabetical order.
-  These pages CAN have a Type::xxxx assigned to use a shape and Color
   from the customizing. Links to these pages may work, but will
   probably screw up the graph :-)
-  add Customizing-Function fchw['zLevels'] for the number or items per
   row for the "Unassigned" Pages.
-  This can be set in LocalSettings.php. If not set, it defaults to 4
   and is set in flowchartwiki.php.
-  Calculate the Height of the graph based on the approximate number of
   rows. (This may not work properly for CategoryBrowser2 which displays
   two images.)

FlowchartWiki 1.1.1
~~~~~~~~~~~~~~~~~~~

-  fixed bug with ' (Apostroph) and " " (Blank) in Category Names. (Many
   thanks to Martin from Tübingen, Germany to report this.)

Upgrading from 1.1.0 to 1.1.1

-  Replace the existing /extensions/flowchartwiki installation with the
   new files. (You may want to backup your existing files first.)
-  delete the contents of the ``./images/flowchartwiki`` directory and
   run
   ``php ./extensions/flowchartwiki/maintenance/fchw_RefreshPages.php``

Upgrading from previous versions to FlowchartWiki 1.1.1:

-  replace the existing /extensions/flowchartwiki installation with the
   new files. (You may want to backup your existing files first.)
-  change the name of the ``./images/fchw`` directory to
   ``./images/flowchartwiki``. Please ensure you keep the proper
   read/write/create/delete authorizations.
-  test your installation with the Special Pages:Check\_Fchw Page.
-  change the customizing pages for the process models to the new
   Version 1.1 format shown in :doc:`Customizing`
-  delete the contents of the ``./images/flowchartwiki`` directory and
   run
   ``php ./extensions/flowchartwiki/maintenance/fchw_RefreshPages.php``

FlowchartWiki 1.1.0
~~~~~~~~~~~~~~~~~~~

-  updated Special Pages:Check\_Fchw
-  changed calls to GraphViz to platform-dependant calls for Unix/Linux,
   Windows-Apache and Windows-IIS platforms
-  fixed calls to GraphViz when there are " "(<space>) characters in the
   path to the ``./images/flowchartwiki`` directory.
-  added $wgDbPrefix to hash for filenames in ./images/flowchartwiki to
   avoid clashes in multi-instance installations
-  added "concentrate=true" to .dot files to combine arrows to/from
   boxes into one arrow.
-  experimental support of Windows-IIS platform.

FlowchartWiki 1.1.0-RC6
~~~~~~~~~~~~~~~~~~~~~~~

-  updated cacheing of images, added "?Timestamp=<timestamp>" to
   image-tag of graph images in order to remove the requirement to press
   "shift-reload" in the browser to see the updated image.
-  Database table access now uses Mediawiki DB-Prefix. (Thanks to
   J.R.M.)
-  Updated special page CheckFchw to show a graph created by graphviz
   for testing graphviz.

FlowchartWiki 1.1.0-RC5
~~~~~~~~~~~~~~~~~~~~~~~

-  Fixed call to GraphViz.exe on Windows installations (fixed problems
   on W2K and W2K3 servers), Unix installs are not affected
-  Added optional ``PageName::DifferentNameForPage`` Tag to override the
   labeling of the box in the Graph (Defaults to Name of WikiPage)
-  BugFix: Quote in PageName
-  BugFix: "\_" are replaced with " " in PageName

FlowchartWiki 1.1.0-RC1
~~~~~~~~~~~~~~~~~~~~~~~

-  Added special page **Special:CheckFchw** to check correct installation
   :doc:`Special_CheckFchw`
-  Fixed bug: Redirected pages causes timeout in some cases
-  New customization format allows to set color, label, shape on arrows
   in graph

Release and upgrade Notes - Current Release PdfBook
---------------------------------------------------

PdfBook is currently broken. It will not work with current MediaWiki
releases due to API-changes in MediaWiki.

PdfBook 1.1.1
~~~~~~~~~~~~~

-  fixed bug with ' (Apostroph) and " " (Blank) in Category names.

Upgrading from previous versions to pdfbook-1.1.1:

-  Replace the existing files with the files of the new version. (You
   may want to backup your existing installation first.)

PdfBook 1.1.0
~~~~~~~~~~~~~

-  updated Special Pages:Check\_Fchw
-  changed calls to htmldoc to platform-dependant calls for Unix/Linux,
   Windows-Apache and Windows-IIS platforms
-  changed unique filename to pdf-book-<uniqueID>
-  deletes temporary files after delivery to user, so
   ``./images/pdfbook`` directory should be mostly empty.
-  fixed calls to htmldoc when there are " "(<space>) characters in the
   path to the ``./images/flowchartwiki`` directory.

PdfBook 1.1.0-RC3
~~~~~~~~~~~~~~~~~

-  minor update to special page Special:CheckPdfBook

PdfBook 1.1.0-RC1
~~~~~~~~~~~~~~~~~

-  Added special page Special:CheckPdfBook to check correct installation
-  PdfBook now should also work when the FlowchartWiki extension is not
   installed or used in a category.

Release 1.0.x
-------------

FlowchartWiki 1.0.1
~~~~~~~~~~~~~~~~~~~

-  Fixed bug: Broken graph if name of pages contains " " space

FlowchartWiki 1.0.0-RC3
~~~~~~~~~~~~~~~~~~~~~~~

-  Added simple cache for graphs
-  Replacement for Hash function (if not exists - on some Solaris hosts)

PdfBook 1.0.0-RC3
~~~~~~~~~~~~~~~~~

-  Sorted book by Level
-  Added support for codepage ISO-8859-2

FlowchartWiki 1.0.0-RC2
~~~~~~~~~~~~~~~~~~~~~~~

-  Fixed for Windows hosts (flowchartwiki and pdfbook too)
-  Fixed duplicate entries in dependencies

FlowchartWiki 1.0.0-RC1
~~~~~~~~~~~~~~~~~~~~~~~

-  First Release of FlowchartWiki.
