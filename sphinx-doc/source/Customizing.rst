Customizing
===========

.. graphviz:: dot/Customizing.dot

Customizing your Installation
-----------------------------

Besides standard options to modify and customize your MediaWiki
installation, FlowchartWiki has some additional customizing options.

Customizing is used to define the shapes and colors used in the diagrams
created by FlowchartWiki. You can create customizing settings for
different types of processes (EPK/Aris, "plain vanilla", ...) and use
them in your process diagrams. Each Process needs to have one
process-type assigned.

The standard distribution of FlowchartWiki includes a set of standard
customizing pages that you can import (described below) and modify/adapt
to your needs. The detailed documentation on how to set up the
customizing is given below as well.

|

Import default customizing
--------------------------

Installation on a remote system
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  Importing the customizing requires an upload of prepared wikipages
   from your local system to the remote system.
-  The pages which will be uploaded are part of the flowchartwiki
   distribution and are located in the flowchartwiki/maintenance
   directory.
-  Download flowchartwiki to your local system or download the
   flowchartwiki/maintenance directory from your server to your local
   system.

Installation on a local system
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  locate the flowchartwiki/maintenance directory in your filesystem (it
   could be in
   ./apache2/htdocs/wiki/extensions/flowchartwiki/maintenance)

Importing the default Customizing
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  In mediawiki choose Special pages/Import pages.
-  Import the file import\_customizing.xml (For sample contents see
   :doc:`Customizing_Configure_Chart`)

Optional imports
~~~~~~~~~~~~~~~~

-  Import the file import\_ShapeTest.xml
-  Import the file import\_FlightBooking.xml


Customizing:Configure\_Chart Page(s)
------------------------------------

See :doc:`Customizing_Configure_Chart` for a Sample.

ModelType determines the Chart Type
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Each Category Page that you use for documenting a Proces needs to
contain one ``ModelType`` Tag like the one shown below.

::

      [[ModelType::EPK]]

``EPK`` defines that for this process the colors and shape types will be
based on the EPK diagram definition in Customizing.

Configure the ModelType / Graphics
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For each ModelType a customizing is required to define the shapes and
colors used. To define the customizing of a ModelType you have two
options:

-  Use an individual Wiki Page (``Customizing:Configure_EPK``) to define
   the customizing for this ModelType.
-  Use the Generic ``Customizing:Configure_Chart`` Wiki Page to define
   the customizing.

The first option has the advantage that this page can easily be exported
and moved to other wikis or be distributed with your process
documentation.

The lookup for Customizing is made in the order:

#. ``Customizing:Configure_[ModelType]``
#. ``Customizing:Configure_Chart``

If no customizing for the specified Modeltype is found, a standard round
shape and Black&White color is used. (You may have seen this when doing
your first install with the "Test" Process. After you have imported the
Customizing, the Diagrams looked different.)

Available Shapes and Colors
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Node-Shapes:
    Not all shape types are supported currently.
    If you would like to test it or have it as a reference on your local
    installation, import the file ``import_ShapeTest.xml`` from the
    maintenance directory of your flowchartwiki distribution.

Colors:
    Please see `[Graphviz-Colors] <http://www.graphviz.org/doc/info/colors.html>`__ for
    the list of available Colors.

Arrows:
    Please see `[Graphviz-arrows] <http://www.graphviz.org/doc/info/arrows.html>`__ for
    the list of available arrows.

    Colors: as above

    Line-Types: solid, dashed (- - -), dotted (.....)

    Label: free text

Using a different Font
^^^^^^^^^^^^^^^^^^^^^^

The font used by graphviz for the text inside the chart boxes can be
changed by modifying the FlowchartWiki php script. (Thanks to Gustav
from Gothenburg, Sweden)

Edit ``categoryBrowser.php`` function ``findPages()``, approx. line 168.

-  from: ``$params="";``
-  to: ``$params="fontname=\"helvetica\", ";``

For available fonts please check the graphviz documentation.
`[Graphviz-Fonts] <http://www.graphviz.org/content/attrs#dfontname>`__

Sample Customizing: Page
------------------------

Sample ``Configure_Chart`` Page:

::

     Some Text with Warning why this page should not be edited.
     == Configuration ==
     *Sample_Configure_ChartType
     **Nodes
     ***PageType Shape Color_of_Shape [Color_of_Font Defaults to Black]
     ***PageType Shape Color_of_Shape Color_of_Font

The Configure\_Chart Page is divided into two Section.

-  The "Warning" Section
-  The "Configuration" Section.

The Warning Section
~~~~~~~~~~~~~~~~~~~

The Warning Section is the initial Text in the page and is not parsed.
It ends at ``== Configuration ==``

The Configuration Section
~~~~~~~~~~~~~~~~~~~~~~~~~

The Configuration Section starts with ``== Configuration ==`` It
contains the definition for one or more chart types.

**Differences between configuration formats**

1.2:

::

     *Configure_<ChartType_1>
     **Nodes
     ***[Category <Shape> <Color_of_Shape>]
     ***<PageType> <Shape> <Color_of_Shape> [<Color_of_Font>, Defaults to Black]
     **Arrows
     ***<LinkType> <Arrow-Shape> <Color_of_Arrow> [<Type_of_line>] [<Arrow_label>]

1.1:

::

     *Configure_<ChartType_1>
     **Nodes
     ***<PageType> <Shape> <Color_of_Shape> [<Color_of_Font>, Defaults to Black]
     **Arrows
     ***<LinkType> <Arrow-Shape> <Color_of_Arrow> [<Type_of_line>] [<Arrow_label>]

1.0:

::

     *Configure_<ChartType_1>
     **<PageType> <Shape> <Color_of_Shape> [<Color_of_Font>, Defaults to Black]


**Version 1.2 Configuration Format**

The definition of a configuration is:

::

     *Configure_<ChartType_1>
     **Nodes
     ***[Category <Shape> <Color_of_Shape>]
     ***<PageType> <Shape> <Color_of_Shape> [<Color_of_Font>, Defaults to Black]
     *Configure_<ChartType_2>
     **Nodes
     ***<PageType> <Shape> <Color_of_Shape> [<Color_of_Font>, Defaults to Black]
     **Arrows
     ***<LinkType> <Arrow-Shape> <Color_of_Arrow> [<Type_of_line>] [<Arrow_label>]

Sample:

::

     *Configure_EPK
     **Nodes
     ***Category   box            red
     ***Event      hexagon        azure3
     ***Decision   diamond        azure3
     ***Function   parallelogram  azure3
     ***DataSource rect           khaki1
     ***Person     box            chartreuse1
     ***Department ellipse        chartreuse1
     ***Product    rect           yellow
     **Arrows
     ***Yes        box            green          dashed      Yes
     ***No         diamond        #ffa0a0        solid       No


-  First line: ``*Configure_<ChartType>``

   -  Only Groups starting with ``*Configure`` are parsed. If you want
      to disable a configuration, renaming it to something different
      (like ``Disabled_Configure`` will disable this configuration
      block.
   -  ``ChartType`` defines the Type of the Chart e.g. "EPK" or "Flat".
      This is the type of the Diagram that is being used.

-  Second line: ``**Nodes``

   -  starts the section where the PageTypes are defined.

-  Definition of PageTypes on the next lines:

   -  **New in 1.2.:**: Optional: ``Category``: When this is included,
      it will show a clickable category-name on top of the chart.
   -  ``PageType`` defines the Pagetypes being used in this Diagram, for
      Example "Person", "Function", "Event", "Decision" etc.
   -  ``Shape`` defines the Shape that is being used. The Definition of
      the shape is taken from the GraphViz Documentation at
      [`[Graphviz - Shapes] <http://www.graphviz.org/doc/info/shapes.html>`__\ ]. Please
      be aware that not all shapes are supported/working.
   -  ``Color_of_Shape`` defines the color that is used to render the
      shape. The name of the colors and a color table can be seen at
      [`[Graphviz - Colors] <http://graphviz.org/doc/info/colors.html>`__\ ]
   -  ``Color_of_Font`` defines that color of the font that is used for
      writing the name of the process-step into the shape. If not given,
      it defaults to black. The same table of colors applies as before.

-  ``**Arrows``

   -  starts the definition block for the arrows.
   -  If the Arrow is defined in the customizing, then the properties in
      the definition are applied. If it is not defined, it defaults to
      "normal" shape, "black" color, "solid" line and no label.

-  Definition of the Arrows on the next lines:

   -  ``<LinkType>`` This is the type of the link that is used in the
      Wikipage. The ``<LinkType>`` has to be defined with the first
      letter capitalized as in "``Yes``\ ". (Thanks to Michael G. from
      Cologne, Germany) Example links to wikipage ``PageName`` with the
      LinkType ``Yes``.

      ::

          [[Yes::PageName]]

   -  ``<Arrow-Shape>`` defines the "endpoint" of the arrow. See
      [`[Graphviz - Arrows] <http://www.graphviz.org/doc/info/arrows.html>`__\ ] for
      details. Defaults to "normal".
   -  ``<Color_of_Arrow>`` the same definition as in the colors of
      shapes applies, defaults to black.
   -  ``[<Type_of_line>]``: solid (default), dashed (----), dotted
      (....)
   -  ``[<Arrow_label>]``: The label that is attached to the line in the
      graph. Currently only one word is supported (no blanks).


**Version 1.0.x Configuration Format**

The definition of a configuration is:

::

     *Configure_<ChartType_1>
     **<PageType> <Shape> <Color_of_Shape> [<Color_of_Font>, Defaults to Black]
     *Configure_<ChartType_2>
     **<PageType> <Shape> <Color_of_Shape> [<Color_of_Font> Defaults to Black]

Sample:

::

     *Configure_EPK
     **Event      hexagon        azure3
     **Decision   diamond        azure3
     **Function   parallelogram  azure3
     **DataSource rect           khaki1
     **Person     box            chartreuse1
     **Department ellipse        chartreuse1
     **Product    rect           yellow

Examples
--------

-  :doc:`Customizing_Configure_Chart` for General Customizing and the sections for EPK and ShapeTest (for all the Shapes in the documentation of GraphViz)

Active configuration Block:

::

     *Configure_EPK
     **Nodes
     ***Event      hexagon        azure3
     ***Decision   diamond        azure3
     ***Function   parallelogram  azure3
     ***DataSource rect           khaki1
     ***Person     box            chartreuse1
     ***Department ellipse        chartreuse1
     ***Product    rect           yellow

Disabled Configuration Block:

::

     *Disabled_Configure_EPK
     **Nodes
     ***Event      hexagon        azure3
     ***Decision   diamond        azure3
     ***Function   parallelogram  azure3
     ***DataSource rect           khaki1
     ***Person     box            chartreuse1
     ***Department ellipse        chartreuse1
     ***Product    rect           yellow
