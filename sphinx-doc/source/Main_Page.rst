FlowchartWiki - Wiki-based Process Modelling and Documentation
==============================================================

.. graphviz:: dot/Main_Page.dot

Flowchartwiki is an extension to MediaWiki for creating flowcharts
from the links between wikipages to support process modelling and
process documentation in MediaWiki. This simplifies the self-organizing
of teams and processes.

**Simple Navigation: click on the auto-generated graphs.**
The diagrams on each page are automatically created from pages in the wiki. Just click on a
Process step in the diagram to go directly to that page.

Use it for

-  Process Diagrams
-  Process Modelling
-  Process Documentation
-  training plans
-  ...What could you think of?

Brief Description
-----------------

FlowchartWiki allows teams and processes to self-organize, using a
Wiki to create process models and process documentation.

Each step within a process is a separate wikipage. And based on the
links between these wikipages and a type assigned to a wikipage, a
flowchart diagram is created automatically.

The diagrams are always up-to-date, which reduces manual maintenance.

Unlike popular office software, all users of the wiki can simultaneously
access the process model and keep accurate for their needs.

FlowchartWiki is an extension to the well-known Mediawiki Software and
both are **free and open source**. Because of this, the powerful
features of Mediawiki, such as the audit trail and notifications, are
brought to bear on easy-to-navigate process documentation.



FlowchartWiki repository on Bitbucket.
------------------------------------------------------------------

-  The FlowchartWiki Git repository is available on BitBucket.org:
   `https://bitbucket.org/tkock/flowchartwiki <https://bitbucket.org/tkock/flowchartwiki>`__
-  To run FlowchartWiki in a Docker-Container please see:
   `https://bitbucket.org/tkock/flowchartwiki-docker <https://bitbucket.org/tkock/flowchartwiki-docker>`__

Update June 2020
----------------

-  Please note: As of June 2020 FlowchartWiki will no longer be maintained by its author. If you want to take over maintenance, feel free to contact the author.


New in 1.2.5
------------

-  Fix deprecations in MediaWiki 1.27 and 1.29.1

New in 1.2.4
------------

-  Fix sloppy coding that PHP 7.x rejects or complains about now.
