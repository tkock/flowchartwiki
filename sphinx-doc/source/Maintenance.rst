Maintenance
===========

.. graphviz:: dot/Maintenance.dot

Re-Initializing the Database
----------------------------

If your FlowchartWiki Database is corrupted, you may re-generate the
table that contains the links between the pages by running the
``fchw\_RefreshPages.php`` script from the commandline.

cd to directory ``htdocs/<yourWiki>``

::

     php ./extensions/flowchartwiki/maintenance/fchw_RefreshPages.php

Windows Note:
    PHP could be started by

::

     c:\Program Files\Apache2\modules\php\php.exe

Unix Note:
    You may need to have the PHP-cli package installed on your system.

Temporary Files
---------------

FlowchartWiki and PDFBook create temporary files in these directories:

::

     ./images/flowchartwiki
     ./images/pdfbook

FlowchartWiki Note:

-  The filenames are created from a hash of the database-prefix and the
   name of the wikipage and are overwritten for each update.
-  There are four files per page:

   -  .dot source which was converted to .png and .map by graphviz.
   -  .png with the graph,
   -  .map with an ImageMap for HTML Display
   -  .dot.md5 which contains the hash value of the dot file and is used
      to validate, if an image needs to be re-created.

-  Deleting these files is safe, they will be recreated at the next
   access, but you will have to delete all 4 files per page.
-  Stale files will only exist for pages that have been deleted, so a
   frequent purging of this directory is not required.

PDFBook Note:

-  There are 2 files per .pdf document

   -  .html source (no Extension) which gets converted to .pdf by
      htmldoc
   -  .pdf with the .pdf document

-  The filenames are 'pdf-book-' with a random number.
-  These Files are created at each creation of a .pdf Document and are
   not re-used.
-  In Pdfbook 1.1.0 the files are deleted after they have been delivered
   to the user, so the directory should be mostly empty.
-  you may want to configure a cron-job to clean up this directory.

Moving to new empty server
--------------------------

-  Export all pages from old server via page Special pages - Export
   pages

   -  First see list of categories, and load pages for each one.
   -  Don't forget to add category page like (Category:Test) and
      MainPage
   -  Don't forget to add customizing pages
      (Customizing:Configure\_Chart,
      Customizing:Configure\_Chart\_Documentation,
      Customizing:Configure\_EPK)

-  New server - choose Special pages / Import pages and select exported
   file
-  Copy your logo to images/logo.png and this line to Localsettings.php

::

     $wgLogo = "/wiki/images/logo.png";

-  Copy Mediawiki:Sidebar page to new location
