.. flowchartwiki documentation master file, created by
   sphinx-quickstart on Thu May 10 09:22:23 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to FlowchartWiki's documentation!
=========================================

.. graphviz:: dot/Main_Page.dot

Please note: As of June 2020 FlowchartWiki will no longer be maintained by its author.
If you want to take over maintenance, feel free to contact the author.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Main_Page
   DecisionMaking
   Download
   Installation
   Customizing
   ImportExistingDocuments
   EnterProcesses
   UserTraining
   Maintenance
   Special_CheckFchw
   Customizing_Configure_Chart_Documentation
   Customizing_Configure_Chart
   Doc-howto
   Contact
   License
