Check FlowChartWiki extension
=============================

Example Screenshot of the ``Special:CheckFchw`` wiki page.

This page will show details about your installation and test all relevant features. 

.. image:: images/Check_Fchw.png
