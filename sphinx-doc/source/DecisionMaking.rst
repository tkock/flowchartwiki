DecisionMaking
==============

.. graphviz:: dot/DecisionMaking.dot

The currently selected Page is "highlighted" with a black background and white font

Free and Open Source
--------------------

-  Both, MediaWiki and the FlowchartWiki Extension, are free and Open
   Source and :doc:`License`
   under the GPL General Public License
-  Download for free, install it and use it for free
-  Modify the source if you wish

5 Minute Introduction to FlowchartWiki - Business
-------------------------------------------------

-  Use a Wiki to simplify the self-organizing of Teams and Business
   Processes
-  Simple and easy to use - very low training requirements
-  make the relevant information available to the person performing the
   task when and where it is needed - instead of hiding it in rarely
   updated binders somewhere hidden on a shelf
-  Team-Members can immediately follow up and contribute which results
   in
-  ongoing and up-to-date documentation of business processes and
   procedures
-  Integrate the process documentation into the daily work - by adding
   checklists and tips & tricks to the documentation
-  Process improvement (Kaizen) is simplified: current documentation is
   available and incremental changes are easily documented and
   implemented
-  Proven MediaWiki Software - used by Wikipedia with thousands of pages
   and millions of users
-  just access it with your browser from any PC - no Software to install
   or distribute
-  simultaneous access - no files to share or distribute, no versions to
   monitor
-  export complete process documentation as a .pdf Document for
   archiving or other purposes
-  access to the documentation may be shared with "extended Valuestream
   Partners" - Customers or Suppliers to streamline the extended
   valuestream
-  easily convert your existing .doc based documentation and import it
   into the wiki

**From a consultants perspective:**

-  enable the client employees to "think process"
-  the work of the consultant in defining and documenting processes is
   immediately visible and transparent to the client
-  due to low training requirements, client employees can be easily
   integrated into the project and can contribute early on by adding
   information or giving feedback
-  easy and early integration of client employees results in client
   buy-in and quick wins

5 Minute Introduction to FlowchartWiki - Technical
--------------------------------------------------

-  Based on `Mediawiki <http://www.mediawiki.org>`__
-  Built on the popular "LAMP" Stack: Linux/Windows/Unix, Apache,
   mySQL/PostgreSQL, PHP
-  FlowchartWiki is implemented as an Extension to MediaWiki and adds
   just one table to the MediaWiki database
-  uses `GraphViz <http://www.graphviz.org>`__ Graph creation Software
   to create the graphs
-  uses htmldoc and an extended pdfbook extension to create .pdf
   documentation of processes
-  implements a set of custom tags to create/display the graphs (see
   "How is it done, exactly?" below)
-  supports hierarchical categories - Processes of Processes (of
   Processes of...)
-  MediaWiki User-Management can be integrated with LDAP or other tools

How is it done, exactly?
------------------------

After installing the Extension and adding a few tags to Category and
Process pages, you are ready to run. This is a brief explanation on how
it is done. Please see the full documentation for all the details.)

A **category page** represents a Process. Three Tags are added to a
category page:

::

      <CategoryBrowser />
      [[ModelType::Draw]]
      [[Type::Process]]

-  **CategoryBrowser**: The CategoryBrowser tag displays the process diagram
   on the current page.
-  **ModelType**: The "ModelType" tag selects a Process Type and defines the
   shapes and colors used for the diagrams. used. (This could be EPK,
   FlowChart, etc. and is fully customizable).
-  **Type::Process** tags this page as being a Process, when added to
   another category as a sub-process.

A **Wiki Page** represents a step within a process. Only a few tags are
added to a single wiki page:

::

      [[NextStep::Customizing]]
      <Dependencies />
      [[Type::Rect_Green]]
      [[Level::0995]]
      [[Category:GettingStarted]]

-  **NextStep::Customizing** tags a Link to a "next process step". NextStep
   could also be "performedBy", "uses", etc. and is freely assignable.
   This describes the type of link to another wikipage. There is no
   limit in linking to other pages.
-  **Dependencies**: This tag creates a table inside the current wikipage
   that shows "who links here" and "where do I link to" including the
   types of links. Using this tag is optional.
-  **Type::Rect\_Green**: Describes the type of this page and determines via
   the customizing what type of shape and color is used for displaying
   this process step in the diagram. Here we used a Modeltype::Draw tag
   in the process definition that is customized with having types like
   Rect\_Green (green rectangle), Rect\_Red, Rect\_Blue. The EPK
   customizing includes types like Event, Decision, Function,
   Datasource, Person, Department, Product, each with its own shape and
   color settings.
-  **Level::0995**: The automatic flowcharting needs some hints on where to
   place the process step into the diagram. We are using a line number
   system. All Process steps with the same line number will show up on
   the same line in the diagram. Higher line numbers will be displayed
   on a lower level. That´s the only option you have to modify the
   diagram - sorry, no more hours spent with beefing up your
   slideshows...
-  **Category:GettingStarted**: This links the process step to the process,
   by linking it to the category page. This tag is standard MediaWiki.

That´s it.

Features
--------

-  Creates Diagrams from the links between WikiPages.
-  All Pages in one category that are tagged will be shown in the
   diagram
-  Hierarchical Processes: One Process can contain other processes, so
   you can drill down to lower levels.
-  Each WikiPage shows the whole process and the current step is marked.
-  configurable display of the process:

   -  whole process with marked step
   -  whole process plus extract of process steps "around" the currently
      selected one

-  customizable display: Instead of "drawing" diagrams, a Type is
   assigned to the process step which determines the shape and color
   used for drawing. This allows a standardisation of the diagrams and
   displays. (Think of EPK/Aris Diagrams or other types of diagrams.)

Optional Features
-----------------

-  Export the whole process documentation to a .pdf by using the PDFbook
   Extension. (Including the graphs) (We currently provide an extended
   version of the original).

Limitations
-----------

-  Charting / GraphViz

   -  FlowchartWiki is using GraphViz to automagically create the
      charts.
   -  GraphViz offers very limited control of chart appearance or
      layout. (We try to give GraphViz some hints by using
      "line-numbers")
   -  Swimlane charts or manual placement of elements is not supported.

-  Cacheing / Performance

   -  FlowchartWiki currently does not support cacheing of pages. It
      will test on each pageload, if the diagram needs to be recreated.

Continue with Installation
--------------------------

-  Test-Drive FlowchartWiki on your own system or your internal network:
   you can freely download and install the software.
-  Download now: :doc:`Download`
