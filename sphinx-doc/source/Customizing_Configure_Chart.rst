Customizing - Configure Chart
=============================

Configure the ModelType / Graphics
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Actual page source, see :doc:`Customizing_Configure_Chart_Documentation` for details:

::

    === Configure the ModelType / Graphics ===
    == Important Info==
    This page is for customizing purposes only.

    <b>Do not edit unless you know what you are doing.</b>

    See [[Customizing:Configure_Chart_Documentation]] for Details on how to configure and edit.

    == Configuration ==
    *Sample_Configure_ChartType
    **Nodes
    ***PageType Shape Color_of_Shape [Color_of_Font Defaults to Black]
    ***PageType Shape Color_of_Shape Color_of_Font


    *Configure_Draw
    **Nodes
    ***Rect_Blue   box            blue
    ***Rect_Yellow rect           yellow
    ***Rect_Red    rect           firebrick2
    ***Rect_Green  rect           forestgreen
    ***Elli_Blue   ellipse        blue
    **Arrows
    ***Install diamond blue solid use_FlowchartWiki
    ***Hosting normal green solid Hosting

    *Configure_ValueStream
    **Nodes
    ***Rect_Red    polygon        cyan3


Actual Page Source of "Customizing:Configure_EPK"

::

    == Configuration ==
    *Configure_EPK
    **Nodes
    ***Category   box            red
    ***Event      hexagon        azure3
    ***Decision   diamond        azure3
    ***Function   parallelogram  azure3
    ***DataSource rect           khaki1
    ***Person     box            chartreuse1
    ***Department ellipse        chartreuse1
    ***Product    rect           yellow
    **Arrows
    ***No diamond blue solid No
    ***Yes normal green solid Yes

Actual Page Source of "Customizing:Configure_ShapeTest"

::

    == Configuration ==
    *Configure_ShapeTest
    **Nodes
    ***Box         box            blue
    ***Polygon     polygon        blue
    ***Ellipse     ellipse        blue
    ***Circle      circle         blue
    ***Point       point          blue
    ***Egg         egg            blue
    ***Triangle    triangle       blue
    ***Plaintext   plaintext      blue
    ***Diamond     diamond        blue
    ***Trapezium   trapezium      blue
    ***Parallelogram parallelogram blue
    ***House         house         blue
    ***Pentagon      pentagon      blue
    ***Hexagon       hexagon       blue
    ***Septagon      septagon      blue
    ***Octagon       octagon       blue
    ***Doublecircle  doublecircle  blue
    ***Doubleoctagon doubleoctagon blue
    ***Tripleoctagon tripleoctagon blue
    ***Invtriangle   invtriangle   blue
    ***Invtrapezium  invtrapezium  blue
    ***Invhouse      invhouse      blue
    ***Mdiamond      mdiamond      blue
    ***Msquare       msquare       blue
    ***Mcircle       mcircle       blue
    ***Rect          rect          blue
    ***Rectangle     rectangle     blue
    ***None          none          blue
    ***Note          note          blue
    ***Tab           tab           blue
    ***Folder        folder        blue
    ***Box3d         box3d         blue
    ***Component     component     blue
