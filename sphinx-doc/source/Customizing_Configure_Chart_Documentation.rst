Customizing: Configure Chart - Documentation
============================================

Configure the ModelType / Graphics
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Documentation for :doc:`Customizing_Configure_Chart`

The Graphics of the process is configured by making changes to the page
:doc:`Customizing_Configure_Chart`

Sample Page
-----------

Sample ``Configure_Chart`` Page:

::

     Some Text with Warning why this page should not be edited.
     == Configuration ==
     *Sample_Configure_ChartType
     **PageType Shape Color_of_Shape [Color_of_Font Defaults to Black]
     **PageType Shape Color_of_Shape Color_of_Font

The Configure\_Chart Page is divided into two Section.

-  The "Warning" Section
-  The "Configuration" Section.

The Warning Section
-------------------

The Warning Section is the initial Text in the page and is not parsed.
It ends at ``== Configuration ==``

The Configuration Section
-------------------------

The Configuration Section starts with ``== Configuration ==`` It
contains the definition for one or more chart types.

The definition of a configuration is:

::

     *Configure_<ChartType_1>
     **<PageType> <Shape> <Color_of_Shape> [<Color_of_Font>, Defaults to Black]
     *Configure_<ChartType_2>
     **<PageType> <Shape> <Color_of_Shape> [<Color_of_Font> Defaults to Black]

Sample:

::

     *Configure_EPK
     **Event      hexagon        azure3
     **Decision   diamond        azure3
     **Function   parallelogram  azure3
     **DataSource rect           khaki1
     **Person     box            chartreuse1
     **Department ellipse        chartreuse1
     **Product    rect           yellow

-  First line: ``*Configure_<ChartType>``

   -  Only Groups starting with ``*Configure`` are parsed. If you want
      to disable a configuration, renaming it to something different
      (like ``Disabled_Configure`` will disable this configuration
      block.
   -  ``ChartType`` defines the Type of the Chart e.g. "EPK" or "Flat".
      This is the type of the Diagram that is being used.

-  Definition of PageTypes on the next lines:

   -  ``PageType`` defines the Pagetypes being used in this Diagram, for
      Example "Person", "Function", "Event", "Decision" etc.
   -  ``Shape`` defines the Shape that is being used. The Definition of
      the shape is taken from the GraphViz Documentation at
      [`[Graphviz - Shapes] <http://www.graphviz.org/doc/info/shapes.html>`__\ ]. Please
      be aware that not all shapes are supported/working.
   -  ``Color_of_Shape`` defines the color that is used to render the
      shape. The name of the colors and a color table can be seen at
      [`[Graphviz - Colors] <http://graphviz.org/doc/info/colors.html>`__\ ]
   -  ``Color_of_Font`` defines that color of the font that is used for
      writing the name of the process-step into the shape. If not given,
      it defaults to black. The same table of colors applies as before.

Examples
--------

Active configuration Block:

::

     *Configure_EPK
     **Event      hexagon        azure3
     **Decision   diamond        azure3
     **Function   parallelogram  azure3
     **DataSource rect           khaki1
     **Person     box            chartreuse1
     **Department ellipse        chartreuse1
     **Product    rect           yellow

Disabled Configuration Block:

::

     *Disabled_Configure_EPK
     **Event      hexagon        azure3
     **Decision   diamond        azure3
     **Function   parallelogram  azure3
     **DataSource rect           khaki1
     **Person     box            chartreuse1
     **Department ellipse        chartreuse1
     **Product    rect           yellow
