Installation
============

.. graphviz:: dot/Installation.dot

System requirements
-------------------

-  Mediawiki system requirements as listed here
   [`[MediaWiki/Installation] <http://www.mediawiki.org/wiki/Installation>`__\ ] with
   PostgreSQL or mySQL database.
-  Parallel usage of Semantik MediaWiki with FlowchartWiki is not
   supported. (Contributions welcome.)

Overview of tested and supported platforms
------------------------------------------

On these platforms we have installed and tested MediaWiki together with
FlowchartWiki. If you use it on other platforms, please let us know so
we can update this table.

+---------------------+----------------------+-------------------+-------------+
| **OS / Database**   | **Windows-Apache**   | **Windows-IIS**   | **Linux**   |
+---------------------+----------------------+-------------------+-------------+
| **Postgresql**      | untested             | untested          | tested OK   |
+---------------------+----------------------+-------------------+-------------+
| **mySQL**           | tested OK            | experimental,     | tested OK   |
|                     |                      | see notes         |             |
+---------------------+----------------------+-------------------+-------------+

Preparation (Windows)
---------------------

-  Install Apache, PHP and mySQL or Postgresql Database.

   -  WAMP (Windows - Apache - mySQL) is pretty easy to set up.
   -  You could use one of the prepacked installers like
      `WinLamp <http://winlamp.sourceforge.net/>`__
   -  Please see the documentation of these installers for more details

Possible issues on Windows:

-  Depending on your environment, GraphViz and HtmlDoc may need addional
   .dll's: vcredist\_x86.exe, msvcr71.dll, libssl (libeay32.dll,
   ssleay32.dll)

   -  GraphViz 2.28 requires the `VC++
      2008 <http://www.microsoft.com/download/en/details.aspx?displaylang=en&id=29>`__
      redistributables, where some older versions required the 2005
      release. (Thanks to Tommy from Hong Kong)

-  Calls from PHP to external programs may be restricted by the OS (This
   is primarily an issue with IIS, see below.)
-  On Windows (XP?) with Apache there seem to be problems in calling the
   htmldoc and graphviz executables when the the DocumentRoot in
   httpd.conf contains " " (Blanks/Spaces). Please move the DocumentRoot
   to a directory path without spaces - like "c:\\htdocs" and update
   DocumentRoot accordingly - to "c:/htdocs". (until 1.1.0-RC6, fixed in
   1.1.0)
-  From PHP 5.4.5 COM is no longer built into php core, you have to add
   COM support in php.ini. See
   `[php installation] <http://www.php.net/manual/en/com.installation.php>`__ (Thanks to Pavel from Moscow, Russia.)
-  Please let us know about your success / problems on Windows
   platforms.

Windows - IIS: (experimental, you have been warned!)

-  Be prepared to do some serious tweaking with rights for IUSR and
   IIS\_WPG. This MAY include rights to ``cmd.exe``, the
   ``c:/program files/graphviz`` and ``c:/program files/htmldoc``
   directories, the ``./images/flowchartwiki`` and ``./images/pdfbook``
   directories - and possibly more.
-  Granting enough rights, we were successfully running
   flowchartwiki/graphviz on W2K3 with IIS.
-  We quit digging deeper into making htmldoc work due to these hints:
   """Enable auditing of 'object failures' in the local security policy,
   see if anything is in Security event log. Another couple of tools to
   use are Regmon and Filemon from Sysinternals. Auditing, Regmon,
   Filemon can narrow down if its a obvious permissions issue. If not,
   it's something in your local security policy preventing it."""
   (Source: `[iis forums] <http://forums.iis.net/p/1147972/1887971.aspx>`__)
-  also tweaking with the calls to the executables in
   ``checkfchw_body.php, checkpdfbook_body.php`` first and then applying
   those changes to ``graphviz.php`` and ``pdfbook.php`` may be
   necessary.
-  IIS seems to handle URLs differently. Please check
   `Manual:Short\_URL <http://www.mediawiki.org/wiki/Manual:Short_URL>`__
   and
   `URL\_rewrite\_in\_IIS <http://meta.wikimedia.org/wiki/URL_rewrite_in_IIS>`__
   and adjust your ``LocalSettings.php`` accordingly. What worked for
   us:

::

     $wgScript           = "/mediawiki/index.php";
     $wgArticlePath      = "$wgScript/$1";
     $wgUsePathInfo      = true;

-  The Special:CheckFchw page seems to have timing issues. It is testing
   for the existence of the generated .png file. The file may be there,
   but windows seems to be slow to recognize this. Please crosscheck the
   ``./images/flowchartwiki`` directory for a ``FchwTest.png`` file.
   This problem may also re-appear in graphviz.php when creating images
   during normal usage. The program already waits, but maybe windows is
   even slower...
-  **Contributions with detailed instructions and fixes are very much
   appreciated.**

Preparation (Unix/Linux)
------------------------

-  For a Docker based installation please see
   `https://bitbucket.org/tkock/flowchartwiki-docker <https://bitbucket.org/tkock/flowchartwiki-docker>`__
-  usually Apache, PHP and mySQL or Postgresql are installed in the base
   installation. If not, please add these packages now.
-  for running php-scripts outside of apache, you may need to add a
   package named php5-cli.

Possible Issues on Linux:

-  Graphviz: (Was reported for CentOS 64bit): apache-user needs access
   to "``/usr/lib64/graphviz/config6``\ " which can be set by granting
   "``chmod 755``\ " on that file. The Error Message in Apache-Log was
   "``Format: "png" not recognized. Use one of:``\ " (Thanks to Gero
   from Boeblingen, Germany)
-  Graphviz: (Was reported for RHEL5): requires GD-support in Graphviz
   which is included in the package "graphviz-**gd**-2.12-8.el5" in
   addition to installing "graphviz-2.12-8.el5". (Thanks to Mangesh)

   -  Test with '/usr/bin/dot -v' and check if the 'render' line
      contains 'png'. Press Ctrl-c to exit dot.

-  Ubuntu: create a ``/var/www/bin`` directory and copy dot and htmldoc
   into it. Then in ``/etc/php5/apache/php.ini`` set
   "``save_mode_exec_dir=/var/www/bin``\ ". In ``LocalSettings.php`` for
   ``$fchw['GraphvizDot']``, just use "``dot``\ " and "``htmldoc``\ "
   respecitively, do not give any path.

Installation steps
------------------

MediaWiki Installation

-  download [`[MediaWiki - Download] <http://www.mediawiki.org/wiki/Download>`__\ ]
-  install [`[MediaWiki - Installation] <http://www.mediawiki.org/wiki/Installation>`__\ ]
   Mediawiki as described in Mediawiki documentation
-  Test your Mediawiki installation

Tip:

-  If you want to run multiple (separate) wikis, use parallel
   installations of mediawiki. These installations can share the same
   database with different $dbprefix settings. FlowchartWiki is not
   compatible with running multiple (separate) wikis in a single
   wiki-installation like "Method One" documented on SteveRumberg.com,
   which switches $dbprefix based on a url-parameter.

GraphViz and Flowchartwiki Extension Installation

-  Download and install Graphviz to your system
   [`[GraphViz] <http://www.graphviz.org/>`__\ ] (The MediaWiki GraphViz
   Extension is not required.)
-  :doc:`Download` and extract Flowchartwiki package to mediawiki/extensions
-  rename the directory from flowchartwiki-x.y.z to flowchartwiki

Database configuration

-  if you use a Database Prefix ($wgDbPrefix), please adjust the
   database scripts accordingly. (see notes in the .sql scripts).
-  Postgres

   -  import ./extensions/flowchartwiki/maintenance/schema\_pg.sql to
      your Postgres database

::

     psql db wikiname < schema_pg.sql

-  mySQL

   -  import ./extensions/flowchartwiki/maintenance/schema\_mysql.sql to
      your mySQL Database

::

     mysql --user=<dbuser> --password=<password> <dbName> < ./extensions/flowchartwiki/maintenance/schema_mysql.sql

Directory setup and Wiki Configuration

-  create directory *flowchartwiki* in *images* subdirectory a set
   permisions on *flowchartwiki* directory, (Please note: The name of
   this directory was changed from ``fchw`` to ``flowchartwiki`` in
   version 1.1.0.)

::

     chmod 777 ./images/flowchartwiki

-  add these lines to mediawiki/LocalSettings.php

   -  note: Set ``<path-to-graphviz-Dot>`` to the path on your system:

      -  Unix/Linux: could be "``/usr/bin/dot``\ "
      -  Windows: could be
         "``C:\\Program Files\\Graphviz 2.20\\bin\\dot.exe"``\ " (use
         double \\\\)

::

     # Disable cache - otherwise graphs are not updated properly
     $wgCachePages       = false;
     $wgCacheEpoch = max( $wgCacheEpoch, gmdate( 'YmdHis' ) );
     # Include libraries
     require_once("$IP/extensions/flowchartwiki/flowchartwiki.php");
     $fchw['GraphvizDot'] = "<path-to-graphviz-Dot>";

Optional installation
---------------------

-  PDF Book - Export your Process Documentation as a .pdf Document
-  PDF Book for FlowchartWiki is currently broken and not maintained.
-  Download HTMLDoc: Open Source: `[htmldoc - OSS] <http://www.htmldoc.org>`__
   Commercial: `[htmldoc - commercial] <http://www.easysw.com/htmldoc/>`__

   -  Unix/Linux: Prebuild packages should be available for your
      distribution.
   -  Windows: Please use Google to locate a compiled open-source
      package or purchase a commercial license from the vendor.

-  Install HTMLDoc
-  Download the FlowchartWiki PdfBook Extension (See :doc:`Download`)
   (This Extension has been modified to work with FlowchartWiki).
-  extract PdfBook mediawiki extension to ./extensions directory
-  rename directory pdfbook-x.y.z to pdfbook
-  create directory *pdfbook* in *images* subdirectory and set
   permisions on *pdfbook* directory,

::

     chmod 777 ./images/pdfbook

-  add these lines to mediawiki/LocalSettings.php and set the correct
   path to the htmldoc executable

::

     require_once("$IP/extensions/pdfbook/pdfbook.php");
     $PdfBookShowTab = true;
     $PdfBookHtmlDoc = "c:\\program files\\htmldoc\\htmldoc.exe";

Additional settings
-------------------

If you want to allow to view pages only for registered users, add these
lines to mediawiki/LocalSettings.php

::

     # Allow only authorized persons
     $wgGroupPermissions['*']['read'] = false;
     $wgGroupPermissions['*']['createaccount'] = false;
     $wgShowExceptionDetails = true;

Parallel usage with Semantic MediaWiki
--------------------------------------

The standard installation of FlowchartWiki will NOT work together with
Semantic MediaWiki, as the style used for tagging the links is the same
as in SMW.

Daniel L. submitted a workaround that uses a different tagging. He is
using "--" for FlowchartWiki instead of "::" (which is used by SMW). If
you want to use FlowchartWiki in parallel with SMW, you may want to give
his modification a try.

Linktypes.php: approx. line 58:

::

     < if (strpos($LinkText, "--") > 0) {
     < $Relation = substr($LinkText, 1, strpos($LinkText, "--")-1);
     < $LinkTmp = substr($LinkText, strpos($LinkText, "--")+2, -1);
     < $output .= "[$LinkTmp|$Relation--$LinkTmp]";
     ---
     > if (strpos($LinkText, "::") > 0) {
     > $Relation = substr($LinkText, 1, strpos($LinkText, "::")-1);
     > $LinkTmp = substr($LinkText, strpos($LinkText, "::")+2, -1);
     > $output .= "[$LinkTmp|$Relation::$LinkTmp]";

Linktypes.php approx. line 164:

::

     < if (strpos($Link, "--") > 0) {
     < $Relation = substr($Link, 0, strpos($Link, "--"));
     < $To_title = substr($Link, strpos($Link, "--")+2);
     ---
     > if (strpos($Link, "::") > 0) {
     > $Relation = substr($Link, 0, strpos($Link, "::"));
     > $To_title = substr($Link, strpos($Link, "::")+2);

Testing your installation
-------------------------

**Special Pages**

Two "special pages" will help you to test and troubleshoot your
installation. (New in Version 1.1.0)

-  :doc:`Special_CheckFchw` (Special:CheckFchw)
-  Check PDFBOOK extension (Special:CheckPdfBook)

These pages check, if everything is installed correctly, including
executables, paths, permissions and will try to create a graph or .pdf
document.

**Set up a "Test Process"**

.. image:: images/TestProcess.png


(After entering the process, this is the diagram you should see on
the WikiPage Test1.)

-  **Step1:** Create category ``Category:Test`` with content (Cut &
   Paste)

::

    <CategoryBrowser />
    [[ModelType::Draw]]

-  **Step 2:** Create Page ``Test1`` with content (Cut & Paste)

::

    <CategoryBrowser />
    [[LinksTo::Test2]]
    <Dependencies />
    [[Type::Rect_Red]]
    [[Level::1000]]
    [[Category:Test]]

-  **Step 3:** Create Page ``Test2`` with content (Cut & Paste)

::

    <CategoryBrowser />
    <Dependencies />
    [[Type::Rect_Blue]]
    [[Level::1010]]
    [[Category:Test]]

-  **Step 4:** Tests:

   -  Open the Category Page ``Category:Test``
      You should see a diagram with two process steps (Test1 and Test2)
      which is clickable and will link to the Pages Test1 and Test2.
      The Diagrams will not be colored or using shapes because the
      customizing for the colors and shapes is not yet available, so
      default values are used. (If you want it pretty, see the
      :doc:`Customizing` documentation).
   -  Click on the diagram on the Pages ``Category:Test`` and ``Test1``,
      ``Test2`` to test the navigation.
   -  you will see some "red" text with the diagrams. This is due to
      rendering the links and expected. (see also on the bottom of this
      page)
   -  you may want to have a look into ``./images/flowchartwiki`` where
      the files for the images are cached.
   -  Testing PdfBook export: Open the category page and click the
      "Export to PDF" tab. You should get an automatic download of a
      .pdf File. The .pdf Document contains all pages from the category,
      graphs included.
