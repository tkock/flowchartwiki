ImportExistingDocuments
=======================

.. graphviz:: dot/ImportExistingDocuments.dot

Easy conversion of .doc Files with OpenOffice.org
-------------------------------------------------

The writer module of OpenOffice.org can open .doc documents and has a
special export filter to export the document in MediaWiki markup.
(=MediaWiki Formatting).

Detailed List of Features:
[`[OpenOffice Features] <http://wiki.services.openoffice.org/wiki/Odt2Wiki/Features>`__\ ]

Procedure
---------

#. Download [`OpenOffice.org <http://www.openoffice.org>`__\ ]
#. Install the downloaded application
#. open the .doc file in the OpenOffice.org Writer module
#. Export the Document in MediaWiki markup as a .txt file

   #. Choose File -> Export
   #. Select File Format "MediaWiki (\*.txt)" and enter a FileName.

#. Open the .txt File in Notepad or any other Text-Editor, mark the
   parts to be transferred or the whole document and copy it into the
   clipboard
#. Create or open the page in your Wiki and go to the Edit-Mode of the
   page
#. Paste the content from the clipboard into the Edit-Window of the
   Wikipage
#. Save the Wikipage
