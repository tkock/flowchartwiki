Documentation HowTo
===================

Building and maintaining the documentation
------------------------------------------

ReadTheDocs.io
~~~~~~~~~~~~~~

The documentation for FlowchartWiki is now hosted on Readthedocs.io on URL:
`https://flowchartwiki.readthedocs.io/en/latest/index.html <https://flowchartwiki.readthedocs.io/en/latest/index.html>`__

Changes to the documentation will be automatically picked up by Readthedocs.

The following documentation is for testing and reviewing the documentation prior to updating the repo.

Requirements
~~~~~~~~~~~~

* Python (3.4+)
* Graphviz to render the images.
* Sphinx with ReadTheDocs theme
* Preferably a Linux box with ``make``

Setup
~~~~~

#. Create a Python virtual environment ``python -m venv venv``
#. Activate the virtual environment ``source venv/bin/activate``
#. Upgrade pip: ``pip install --upgrade pip``
#. Install required Python packages into the virtual environment ``pip install sphinx sphinx_rtd_theme`` or use
   ``pip install -r requirements.txt``


Building the documentation
~~~~~~~~~~~~~~~~~~~~~~~~~~

#. Build documentation ``make html``
#. Build .epub ``make epub``

Extraction of original source documents
---------------------------------------

The original source documentation was in a FlowchartWiki based MediaWiki installation.

Most of the pages have been extracted in two formats:

* **MediaWiki markup**, extracted by manually copying the markup from the "edit page" option.
* **.html markup** extracted by using ``wget "http://www.flowchartwiki.org/wiki/index.php?title=Main_Page&action=render" -O Main_Page.html``

(These files have been retained in the ``source/original`` folder.)

The .html file was then converted to restructuredText by using pandoc::

    pandoc -f html -t rst main.html -o main.rst

Finally the .rst file was manually updated and polished.
