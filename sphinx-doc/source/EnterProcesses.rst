EnterProcesses
==============

.. graphviz:: dot/EnterProcesses.dot

How to create a process
-----------------------

Sample Category Page Content:

::

    <CategoryBrowser />

    Description of the process...

    configure the type of the graphics
      '[ModelType::EPK]
      '[ModelType::Flat]
    [[ModelType::EPK]]

-  CategoryBrowser - Tag displays graph of current category. Items are
   process steps of this category.
-  ModelType::EPK - Specifies graphics style for graph, definition from
   this page is used for all process step graphs.

How to create a process step
----------------------------

Sample page content:

::

    <CategoryBrowser />

    Description of the process step...

    [[UsedBy::CheckBookingRequest]]

    <Dependencies />

    [[Type::Product]]
    [[Level::1010]]
    [[PageName::DifferentName]]
    [[Category:Flightbooking]]

-  ``CategoryBrowser`` tag displays graph of current category. Items are
   process steps of this category. Note: that you can use
   CategoryBrowser2 tag displays 2 graphs - left shows whole process of
   current step, and right only neighbours.
-  ``Dependencies`` tag shows table with dependencies between process
   steps
-  ``Type::Product`` - Type for current page (displays specified
   shape/color in graph)
-  ``Level::1010`` - Steps with same level are in same line in graph.
   Increase the Level by 10 for the next line in the graph - to
   Level::1020, Level::1030 etc. (If you have done some BASIC
   Programming "in the good old days" - this is like line-numbers in
   Basic. - And yes, we are working on a renumbering function ;-))
-  ``PageName::DifferentName`` (optional) - this overrides the usage of
   the name of the WikiPage in the Graph with "DifferentName". i.e. If
   your WikiPage is named "SomePage" - it will default to the box
   labeled "SomePage" in the Graph. If you use
   "PageName::Some\_Other\_Name\_For\_This\_Page" it will be labeled
   "Some Other Name For This Page" in the Graph. ("\_" are replaced with
   " "). To split the text into multiple lines, insert "\\n" where a
   linebreak should be placed. Example:
   "PageName::Some\_Other\_Name\ **\\n**\ For\_This\_Page" would show
   the label in two lines: 1:"Some Other Name", 2: "For This Page".
-  ``Category:Flightbooking`` - This is important. Describes
   participation in process.

Detail documentation
--------------------

Tag CategoryBrowser
~~~~~~~~~~~~~~~~~~~

::

    <CategoryBrowser />

Displays 1 graph.

::

    <CategoryBrowser2 />

Displays 2 graphs. Left shows whole process and right only neighbours.
CategoryBrowser2 on category page show only 1 graph.

::

    <CategoryBrowser>Name_Of_Category</CategoryBrowser>
    <CategoryBrowser2>Name_Of_Category</CategoryBrowser2>

Displays graph(s) for selected category (graph of another category
process).

Tag Dependencies
~~~~~~~~~~~~~~~~

::

    <Dependencies />

Shows table with dependencies including type of links.

.. image:: images/DependenciesTag.png


Tips & Tricks
-------------

**FCKEditor**:

If you use the FCKEditor with FlowchartWiki, you need to put
``__NORICHEDITOR__`` at the start of each page.
Otherwise FCKEditor may mangle the Tags like this: (Thanks to Andrew
from New Zealand.)

::

      [[Type::Rect_Red]]                     # original Tag
      [[Rect Red|Type::Rect_Red]]            # 1st round
      [[Rect Red|Rect Red|Type::Rect_Red]]   # 2nd round

**Positioning of the Tags**:

You may want to place all the FlowchartWiki related tags (like
"NextStep::Maintenance", "Type::Rect\_Green", "Level::1030" etc.) to the
end of the page, if your users are disturbed by the tags showing up in
the middle of the text.
