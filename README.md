# FlowchartWiki #

License: GNU General Public License, Version 2 (or later).

Copyright: Thomas Kock, Delmenhorst, Germany, 2008 - 2020

Flowchartwiki is an extension to MediaWiki for creating flowcharts from the
links between wikipages to support process modelling and process documentation
in MediaWiki. This simplifies the self-organizing of teams and processes.

As of June 2020 FlowchartWiki will no longer be maintained by its author.
If you want to take over maintenance, feel free to contact the author.

[Documentation](https://flowchartwiki.readthedocs.io/en/latest/index.html)

